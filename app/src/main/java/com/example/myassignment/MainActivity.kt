package com.example.myassignment

import android.os.Bundle
import android.text.Selection.setSelection
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myassignment.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var binding : ActivityMainBinding? = null
    private var customAdapter : CustomAdapter? = null
    private var dataList : ArrayList<String>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setupBinding()
        init()
    }

    private fun setupBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
    }

    private fun init() {
      dataList = ArrayList<String>()

      binding!!.clkBtn.setOnClickListener(object : View.OnClickListener {
          override fun onClick(p0: View?) {




              val alertDialog : AlertDialog = AlertDialog.Builder(this@MainActivity).create()
              alertDialog.setTitle("Type Something...")
              alertDialog.setCancelable(false)

              val rootView : View? = LayoutInflater.from(this@MainActivity).inflate((R.layout.layout_custom_dialog), null)
              val getInfo : EditText= rootView!!.findViewById(R.id.textHere)
              val getInfo2 : EditText= rootView.findViewById(R.id.text2)
              val yes : Button= rootView.findViewById(R.id.yes)


              alertDialog.setView(rootView)

              yes.setOnClickListener(object : View.OnClickListener{
                  override fun onClick(p0: View?) {
                      val value : String = getInfo.text.toString()
                      val value2 : String= getInfo2.text.toString()
                      dataList!!.add(value)
                      dataList!!.add(value2)


                      callOfCustomAdapter()

                      alertDialog.dismiss()
                  }
              })

              alertDialog.show()
          }
      })

        refreshCustomAdapter()
          }

    private fun callOfCustomAdapter(){
        customAdapter = CustomAdapter(this@MainActivity, dataList!!, object : CustomAdapter.CallBacks{
            override fun callOfDeletion(position: Int) {
               //textHere.setSelection(position)

                dataList!!.removeAt(position)
                customAdapter!!.notifyItemChanged(position)
                refreshCustomAdapter()
            }
        })
        refreshCustomAdapter()
    }

    private fun refreshCustomAdapter(){
        binding!!.recyclerViews.layoutManager = LinearLayoutManager(this)
        binding!!.recyclerViews.adapter = customAdapter
    }
}
