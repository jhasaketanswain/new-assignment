package com.example.myassignment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter (private val context: Context, private val dataList : ArrayList<String>,
        private val myCalls : CallBacks) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    interface CallBacks {
        fun callOfDeletion(position: Int)
    }
    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val textToShow : TextView= itemView.findViewById(R.id.textView)
        val delete : Button= itemView.findViewById(R.id.delete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val customAdapterView : View? = LayoutInflater.from(parent.context).inflate(R.layout.layout_custom_adapter, parent, false)
        return ViewHolder(customAdapterView!!)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listOfData = dataList[position]

        holder.textToShow.text = listOfData

        holder.delete.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                myCalls.callOfDeletion(holder.adapterPosition)
            }
        })
    }
    override fun getItemCount(): Int {
        return dataList.size
    }

}